package com.bnt.weatherapp.util

import android.content.SharedPreferences
import com.f2prateek.rx.preferences2.RxSharedPreferences
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrefsHelper @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {
    companion object {
        const val LAT_KEY = "lat"
        const val LNG_KEY = "lng"
    }

    private val rxSharedPreferences = RxSharedPreferences.create(sharedPreferences)
    private val latPref = rxSharedPreferences.getLong(LAT_KEY)
    private val lngPref = rxSharedPreferences.getLong(LNG_KEY)

    fun writeLatLng(lat: Double, lng: Double) =
        writeLat(lat).andThen(writeLng(lng))

    fun writeLat(lat: Double) =
        Completable.fromAction {
            latPref.set(lat.toRawBits())
        }

    fun writeLng(lng: Double) =
        Completable.fromAction {
            rxSharedPreferences.getLong(LNG_KEY).set(lng.toRawBits())
        }

    fun observLat(): Flowable<Double> {
        return latPref.asObservable()
            .toFlowable(BackpressureStrategy.LATEST)
            .map { Double.fromBits(it) }
    }

    fun observLng(): Flowable<Double> {
        return lngPref.asObservable()
            .toFlowable(BackpressureStrategy.LATEST)
            .map { Double.fromBits(it) }
    }
}