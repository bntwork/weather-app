package com.bnt.weatherapp.util

import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkUtil @Inject constructor() {
    fun isAvailable(): Boolean {
        try {
            val pingResult = Runtime.getRuntime()
                .exec("/system/bin/ping -c 1 -w 0.1 8.8.8.8")
                .waitFor()
            return pingResult == 0
        } catch (ex: IOException) {
            ex.printStackTrace()
        } catch (ex: InterruptedException) {
            ex.printStackTrace()
        }
        return false
    }
}