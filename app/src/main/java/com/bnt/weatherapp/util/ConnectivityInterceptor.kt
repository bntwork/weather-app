package com.bnt.weatherapp.util

import com.bnt.weatherapp.exception.NetworkUnavailableException
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityInterceptor @Inject constructor(
    private val networkUtil: NetworkUtil
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        if (networkUtil.isAvailable()) {
            return chain.proceed(chain.request())
        } else {
            throw NetworkUnavailableException()
        }
    }
}