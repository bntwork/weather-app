package com.bnt.weatherapp.util

import android.support.annotation.DrawableRes
import android.support.v7.content.res.AppCompatResources
import android.widget.TextView

fun TextView.setCompoundDrawables(
    @DrawableRes startDrawableRes: Int? = null,
    @DrawableRes topDrawableRes: Int? = null,
    @DrawableRes endDrawableRes: Int? = null,
    @DrawableRes bottomDrawableRes: Int? = null
) {
    setCompoundDrawablesRelativeWithIntrinsicBounds(
        when {
            startDrawableRes != null -> AppCompatResources.getDrawable(context, startDrawableRes)
            else -> compoundDrawablesRelative[0]
        },
        when {
            topDrawableRes != null -> AppCompatResources.getDrawable(context, topDrawableRes)
            else -> compoundDrawablesRelative[1]
        },
        when {
            endDrawableRes != null -> AppCompatResources.getDrawable(context, endDrawableRes)
            else -> compoundDrawablesRelative[2]
        },
        when {
            bottomDrawableRes != null -> AppCompatResources.getDrawable(context, bottomDrawableRes)
            else -> compoundDrawablesRelative[3]
        }
    )
}