package com.bnt.weatherapp.util

import com.bnt.weatherapp.Const
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response =
        chain.proceed(
            chain.request()
                .newBuilder()
                .url(
                    chain.request()
                        .url()
                        .newBuilder()
                        .addQueryParameter(Const.Query.APPID, Const.APP_ID)
                        .addQueryParameter(Const.Query.UNITS, Const.Units.METRIC)
                        .build()
                )
                .build()
        )
}