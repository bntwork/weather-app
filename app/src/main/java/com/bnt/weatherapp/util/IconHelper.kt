package com.bnt.weatherapp.util

import android.support.annotation.DrawableRes
import com.bnt.weatherapp.R

class IconHelper {
    companion object {
        @DrawableRes
        fun getWeatherIconId(icon: String) =
            when (icon) {
                // day
                "01d" -> R.drawable.ic_white_day_bright
                "02d" -> R.drawable.ic_white_day_cloudy
                "03d" -> R.drawable.ic_white_day_cloudy
                "04d" -> R.drawable.ic_white_day_cloudy
                "09d" -> R.drawable.ic_white_day_shower
                "10d" -> R.drawable.ic_white_day_rain
                "11d" -> R.drawable.ic_white_day_thunder
                "13d" -> R.drawable.ic_show
                "50d" -> R.drawable.ic_white_night_cloudy
                // night
                "01n" -> R.drawable.ic_white_night_bright
                "02n" -> R.drawable.ic_white_night_cloudy
                "03n" -> R.drawable.ic_white_night_cloudy
                "04n" -> R.drawable.ic_white_night_cloudy
                "09n" -> R.drawable.ic_white_night_shower
                "10n" -> R.drawable.ic_white_night_rain
                "11n" -> R.drawable.ic_white_night_thunder
                "13n" -> R.drawable.ic_show
                "50n" -> R.drawable.ic_white_night_cloudy
                else -> R.drawable.ic_icon_wind_e
            }

        @DrawableRes
        fun getWindDirectionIconId(degree: Double) =
            when {
                degree >= 22.5 && degree < 67.5 -> R.drawable.ic_icon_wind_ne
                degree >= 67.5 && degree < 112.5 -> R.drawable.ic_icon_wind_e
                degree >= 112.5 && degree < 157.5 -> R.drawable.ic_icon_wind_se
                degree >= 157.5 && degree < 202.5 -> R.drawable.ic_icon_wind_s
                degree >= 202.5 && degree < 247.5 -> R.drawable.ic_icon_wind_ws
                degree >= 247.5 && degree < 292.5 -> R.drawable.ic_icon_wind_w
                degree >= 292.5 && degree < 337.5 -> R.drawable.ic_icon_wind_wn
                (degree >= 337.5 && degree < 360.0) || (degree >= 0 && degree < 22.5) -> R.drawable.ic_icon_wind_n
                else -> R.drawable.ic_humidity
            }
    }
}