package com.bnt.weatherapp.util

import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DateFormatter @Inject constructor(
    private val locale: Locale
) {
    private val dateFormatter: SimpleDateFormat = SimpleDateFormat("", locale)

    fun secondsToDayOfWeekDayMonth(date: Long): String {
        val date1 = Date(date * 1000)

        dateFormatter.applyPattern("E")
        val dayOfWeek = dateFormatter.format(date1).toUpperCase()

        dateFormatter.applyPattern(", dd MMMM")
        val dayOfMonthAndMonthName = dateFormatter.format(date1)

        return dayOfWeek + dayOfMonthAndMonthName
    }

    fun getHour(date: Long): String {
        dateFormatter.applyPattern("HH")
        return dateFormatter.format(Date(date * 1000L))
    }

    fun getDayOfWeek(date: Long): String {
        dateFormatter.applyPattern("E")
        return dateFormatter.format(Date(date * 1000L))
    }
}