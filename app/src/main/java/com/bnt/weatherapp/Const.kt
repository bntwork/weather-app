package com.bnt.weatherapp

class Const {
    companion object {
        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val APP_ID = "2bdd941252df62b65d1bb284c46eae4a"
        const val PREFS_NAME = "weather_prefs"
    }

    class Query {
        companion object {
            const val APPID = "appid"
            const val UNITS = "units"
            const val LAT = "lat"
            const val LON = "lon"
        }
    }

    class Units {
        companion object {
            const val METRIC = "metric"
        }
    }
}