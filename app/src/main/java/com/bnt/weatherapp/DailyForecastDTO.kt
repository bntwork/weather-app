package com.bnt.weatherapp

data class DailyForecastDTO(
    val id: Long,
    val dayOfWeek: String,
    val minTemp: Int,
    val maxTemp: Int,
    val clouds: String,
    val hourlyForecasts: List<HourForecastDTO>
)