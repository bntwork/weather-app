package com.bnt.weatherapp

import android.app.Application
import com.bnt.weatherapp.dagger.AppComponent
import com.bnt.weatherapp.dagger.DaggerAppComponent
import com.bnt.weatherapp.dagger.module.AppModule
import com.bnt.weatherapp.dagger.module.NetworkModule
import com.bnt.weatherapp.dagger.module.PrefsModule

class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule(Const.BASE_URL))
            .prefsModule(PrefsModule(Const.PREFS_NAME))
            .build()
    }
}