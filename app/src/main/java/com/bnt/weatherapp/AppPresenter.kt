package com.bnt.weatherapp

import com.arellomobile.mvp.InjectViewState
import com.bnt.weatherapp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class AppPresenter : BasePresenter<AppView>() {

    @Inject
    lateinit var router: Router

    init {
        App.appComponent.inject(this)
    }

    fun start() {
        router.newRootScreen(Screens.DashboardScreen())
    }
}