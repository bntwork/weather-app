package com.bnt.weatherapp

import com.bnt.weatherapp.dashboard.DashboardFragment
import com.bnt.weatherapp.search.SearchFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {
    class DashboardScreen : SupportAppScreen() {
        override fun getFragment() = DashboardFragment()
    }

    class SearchScreen : SupportAppScreen() {
        override fun getFragment() = SearchFragment()
    }
}