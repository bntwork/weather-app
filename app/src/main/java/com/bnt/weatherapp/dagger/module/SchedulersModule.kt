package com.bnt.weatherapp.dagger.module

import com.bnt.weatherapp.util.AppSchedulerProvider
import com.bnt.weatherapp.util.SchedulerProvider
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class SchedulersModule {
    @Binds
    @Singleton
    abstract fun provideSchedulerProvider(provider: AppSchedulerProvider): SchedulerProvider
}