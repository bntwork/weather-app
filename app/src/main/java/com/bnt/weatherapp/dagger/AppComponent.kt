package com.bnt.weatherapp.dagger

import com.bnt.weatherapp.AppActivity
import com.bnt.weatherapp.AppPresenter
import com.bnt.weatherapp.dagger.module.*
import com.bnt.weatherapp.dashboard.DashboardFragment
import com.bnt.weatherapp.dashboard.DashboardPresenter
import com.bnt.weatherapp.search.SearchFragment
import com.bnt.weatherapp.search.SearchPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        NavigationModule::class,
        SchedulersModule::class,
        LocaleModule::class,
        PrefsModule::class,
        GeocoderModule::class
    ]
)
interface AppComponent {
    fun inject(activity: AppActivity)
    fun inject(presenter: AppPresenter)
    fun inject(presenter: DashboardPresenter)
    fun inject(fragment: SearchFragment)
    fun inject(presenter: SearchPresenter)
    fun inject(fragment: DashboardFragment)
}