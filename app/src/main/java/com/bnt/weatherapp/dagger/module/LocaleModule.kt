package com.bnt.weatherapp.dagger.module

import dagger.Module
import dagger.Provides
import java.util.*
import javax.inject.Singleton

@Module
class LocaleModule {
    @Provides
    @Singleton
    fun providesLocale() = Locale("ru", "ua")
}