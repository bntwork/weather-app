package com.bnt.weatherapp.dagger.module

import android.content.Context
import android.location.Geocoder
import dagger.Module
import dagger.Provides
import java.util.*
import javax.inject.Singleton

@Module
class GeocoderModule {
    @Provides
    @Singleton
    fun providesGeocoder(context: Context, locale: Locale) =
        Geocoder(context, locale)
}