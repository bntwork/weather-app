package com.bnt.weatherapp.dagger.module

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PrefsModule(private val prefsName: String) {
    @Provides
    @Singleton
    fun providesSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
}