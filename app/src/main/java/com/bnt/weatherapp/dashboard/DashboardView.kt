package com.bnt.weatherapp.dashboard

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.bnt.weatherapp.DailyForecastDTO
import com.bnt.weatherapp.HourForecastDTO

interface DashboardView : MvpView {
    fun setCityName(cityName: String)
    fun setCityName(lat: Double, lon: Double)
    fun setDate(date: String)
    fun setMaxMinTemp(max: Int, min: Int)
    fun setHumidity(humidity: Int)
    fun setWindSpeedAndDirection(speed: Int, degree: Double)
    fun setHourlyForecast(hourlyForecast: List<HourForecastDTO>)
    fun setDailyForecast(dailyForecast: List<DailyForecastDTO>)
    fun setWeatherTypeImage(weatherType: String)
    @StateStrategyType(SkipStrategy::class)
    fun showError(errorMessage: String)
    @StateStrategyType(SkipStrategy::class)
    fun launchLocationRetrieve()
}
