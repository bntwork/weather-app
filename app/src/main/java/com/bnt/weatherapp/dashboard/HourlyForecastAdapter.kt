package com.bnt.weatherapp.dashboard

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bnt.weatherapp.HourForecastDTO
import com.bnt.weatherapp.R
import com.bnt.weatherapp.util.IconHelper
import kotlinx.android.synthetic.main.hourly_forecast_item.view.*

class HourlyForecastAdapter(
    private var list: List<HourForecastDTO> = ArrayList()
) : RecyclerView.Adapter<HourlyForecastAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.hourly_forecast_item,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val view = viewHolder.itemView
        val hourForecast = list[position]

        view.hourTextView.text = String.format("%02d", hourForecast.hour)
        view.cloudsImageView.setImageResource(IconHelper.getWeatherIconId(hourForecast.clouds))
        view.tempTextView.text = String.format(
            view.context.getString(R.string.temp_format),
            hourForecast.temp
        )
    }

    fun setItems(hourlyForecast: List<HourForecastDTO>) {
        this.list = hourlyForecast
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}