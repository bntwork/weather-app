package com.bnt.weatherapp.dashboard

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.bnt.weatherapp.App
import com.bnt.weatherapp.DailyForecastDTO
import com.bnt.weatherapp.HourForecastDTO
import com.bnt.weatherapp.Screens
import com.bnt.weatherapp.api.WeatherService
import com.bnt.weatherapp.api.entity.WeathItem
import com.bnt.weatherapp.api.entity.WeatherResponse
import com.bnt.weatherapp.base.BasePresenter
import com.bnt.weatherapp.exception.NetworkUnavailableException
import com.bnt.weatherapp.util.DateFormatter
import com.bnt.weatherapp.util.PrefsHelper
import com.bnt.weatherapp.util.SchedulerProvider
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import ru.terrakok.cicerone.Router
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

@InjectViewState
class DashboardPresenter : BasePresenter<DashboardView>() {

    @Inject
    lateinit var weatherService: WeatherService
    @Inject
    lateinit var dateFormatter: DateFormatter
    @Inject
    lateinit var schedulerProvider: SchedulerProvider
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var prefsHelper: PrefsHelper

    init {
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        addDisposable(
            Flowable.zip(
                prefsHelper.observLat(),
                prefsHelper.observLng(),
                BiFunction<Double, Double, Single<WeatherResponse>> { lat, lng ->
                    weatherService.getForecastByCoords(lat, lng)
                        .onErrorResumeNext {
                            Single.just(WeatherResponse())
                        }
                }
            )
                .observeOn(schedulerProvider.io())
                .flatMapSingle { it }
                .observeOn(schedulerProvider.ui())
                .doOnError { throwable ->
                    throwable.printStackTrace()
                    if (throwable is NetworkUnavailableException) {
                        viewState.showError(throwable.message)
                    }
                }
                .observeOn(schedulerProvider.io())
                .flatMapSingle { resp ->
                    Flowable.fromIterable(resp.list)
                        .toSortedList { o1, o2 -> o1.dt.compareTo(o2.dt) }
                        .map { resp.copy(list = it) }
                }//sorted list
                .observeOn(schedulerProvider.ui())
                .doOnNext { viewState.setCityName(it.city.coord.lat, it.city.coord.lon) }
                .observeOn(schedulerProvider.io())
                .filter { it.list.isNotEmpty() }
                .flatMapSingle { resp ->
                    val startDateMillis = resp.list[0].dt * 1000
                    val sdf = SimpleDateFormat("dd.MM.yyyy")
//                    sdf.timeZone = TimeZone.getTimeZone("UTC")
                    var lastSavedDate = sdf.format(Date(startDateMillis))
                    var weathPart = ArrayList<WeathItem>()
                    var restByDate = ArrayList<WeatherResponse>()
                    var i = 0
                    while (i < resp.list.size) {
                        val weathItem = resp.list[i]
                        val formatDt = sdf.format(Date(weathItem.dt * 1000))
                        if (lastSavedDate == formatDt) {
                            weathPart.add(weathItem)
                            if (i == resp.list.size - 1) {
                                restByDate.add(
                                    resp.copy(list = weathPart.toList())
                                )
                            }
                            i++
                        } else {
                            lastSavedDate = formatDt
                            restByDate.add(
                                resp.copy(list = weathPart.toList())
                            )
                            weathPart.clear()
                        }
                    }
                    Flowable.fromIterable(restByDate)
                        .flatMapSingle { resp1 ->
                            Flowable.fromIterable(resp1.list)
                                .map {
                                    HourForecastDTO(
                                        it.dt,
                                        dateFormatter.getHour(it.dt).toInt(),
                                        it.weather[0].icon,
                                        it.wind.speed.roundToInt(),
                                        it.wind.deg,
                                        it.main.humidity,
                                        it.main.temp.roundToInt()
                                    )
                                }
                                .toList()
                                .map {
                                    DailyForecastDTO(
                                        0,
                                        dateFormatter.getDayOfWeek(it[0].dt).toUpperCase(),
                                        it.minBy { it.temp }?.temp ?: 0,
                                        it.maxBy { it.temp }?.temp ?: 0,
                                        it[((it.size - 1) / 2.0).roundToInt()].clouds,
                                        it
                                    )
                                }

                        }
                        .toList()
                }
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    { dtos ->
                        viewState.setDailyForecast(dtos)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        if (throwable is NetworkUnavailableException) {
                            viewState.showError(throwable.message)
                        }
                    }
                )
        )
    }

    fun loadWeatherData(latitude: Double?, longitude: Double?) {
        addDisposable(
            prefsHelper.writeLatLng(latitude ?: 0.0, longitude ?: 0.0)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        Log.e("lskdjflksdf", "LOCATION CHANGED")
                    },
                    { throwable ->
                        throwable.printStackTrace()
                    }
                )
        )
    }

    fun onDayClicked(dailyForecast: DailyForecastDTO) {
        viewState.setHourlyForecast(dailyForecast.hourlyForecasts)
        viewState.setDate(dateFormatter.secondsToDayOfWeekDayMonth(dailyForecast.hourlyForecasts[0].dt))
        viewState.setMaxMinTemp(dailyForecast.maxTemp, dailyForecast.minTemp)

        val middleOfForecast = dailyForecast.hourlyForecasts[dailyForecast.hourlyForecasts.size / 2]
        viewState.setHumidity(middleOfForecast.humidity)
        viewState.setWindSpeedAndDirection(
            middleOfForecast.windSpeed,
            middleOfForecast.windDirection
        )
        viewState.setWeatherTypeImage(middleOfForecast.clouds)
    }

    fun onCityNameClicked() {
        router.navigateTo(Screens.SearchScreen())
    }

    fun onMyLocationClicked() {
        viewState.launchLocationRetrieve()
    }
}