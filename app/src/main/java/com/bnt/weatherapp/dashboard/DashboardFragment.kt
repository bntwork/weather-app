package com.bnt.weatherapp.dashboard

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bnt.weatherapp.App
import com.bnt.weatherapp.DailyForecastDTO
import com.bnt.weatherapp.HourForecastDTO
import com.bnt.weatherapp.R
import com.bnt.weatherapp.base.BaseFragment
import com.bnt.weatherapp.util.IconHelper
import com.bnt.weatherapp.util.setCompoundDrawables
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.PlacePicker
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.fragment_dashboard.*
import java.io.IOException
import javax.inject.Inject

class DashboardFragment : BaseFragment(), DashboardView {

    override val layoutId: Int = R.layout.fragment_dashboard
    private val hourlyForecastAdapter = HourlyForecastAdapter()
    private val dailyForecastAdapter = DailyForecastAdapter()
    @InjectPresenter
    lateinit var presenter: DashboardPresenter
    private val rxPermissions by lazy { RxPermissions(this) }
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var lastSelectedItem: Int? = 0

    private val PLACE_PICKER_REQUEST = 5342
    private val SELECTION_POSITION_ARG = "selection_position"

    @Inject
    lateinit var geocoder: Geocoder

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SELECTION_POSITION_ARG, dailyForecastAdapter.lastSelectedPosition)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        lastSelectedItem = savedInstanceState?.getInt(SELECTION_POSITION_ARG)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        maxMinTempTextView.setCompoundDrawables(R.drawable.ic_temp)
        humidityTextView.setCompoundDrawables(R.drawable.ic_humidity)
        windSpeedTextView.setCompoundDrawables(
            startDrawableRes = R.drawable.ic_wind,
            endDrawableRes = R.drawable.ic_icon_wind_n
        )

        hourlyForecastRecyclerView.adapter = hourlyForecastAdapter
        dailyForecastRecyclerView.adapter = dailyForecastAdapter

        dailyForecastAdapter.setOnItemClickListener(presenter::onDayClicked)
        locationNameTextView.setOnClickListener {
            presenter.onCityNameClicked()
        }

        myLocationImageView.setOnClickListener { presenter.onMyLocationClicked() }
        pickLocationImageView.setOnClickListener { startPlacePicker() }
    }

    private fun startPlacePicker() {
        val placePickerIntent = PlacePicker.IntentBuilder().build(requireActivity())
        startActivityForResult(placePickerIntent, PLACE_PICKER_REQUEST)
    }

    override fun launchLocationRetrieve() {
        val rotateAnimation =
            RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotateAnimation.duration = 1000
        myLocationImageView.startAnimation(rotateAnimation)
        rxPermissions.requestEachCombined(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
            .subscribe { permission ->
                when {
                    permission.granted -> {
                        // proceed
                        fusedLocationClient.lastLocation
                            .addOnSuccessListener { location: Location? ->
                                presenter.loadWeatherData(location?.latitude, location?.longitude)
                            }
                    }
                    permission.shouldShowRequestPermissionRationale -> {
                        // Denied permission without ask never again
                        presenter.loadWeatherData(0.0, 0.0)
                    }
                    else -> {
                        // Denied permission with ask never again
                        // Need to go to the settings
                        presenter.loadWeatherData(0.0, 0.0)
                    }
                }
            }
    }

    override fun setCityName(cityName: String) {
        locationNameTextView.text = cityName
    }

    override fun setDate(date: String) {
        dateTextView.text = date
    }

    override fun setMaxMinTemp(max: Int, min: Int) {
        maxMinTempTextView.text = getString(R.string.max_min_temp_format).format(max, min)
    }

    override fun setHumidity(humidity: Int) {
        humidityTextView.text = getString(R.string.humidity_format).format(humidity)
    }

    override fun setWindSpeedAndDirection(speed: Int, degree: Double) {
        windSpeedTextView.text = getString(R.string.wind_speed_format).format(speed)
        windSpeedTextView.setCompoundDrawables(endDrawableRes = IconHelper.getWindDirectionIconId(degree))
    }

    override fun setHourlyForecast(hourlyForecast: List<HourForecastDTO>) {
        hourlyForecastAdapter.setItems(hourlyForecast)
    }

    override fun setDailyForecast(dailyForecast: List<DailyForecastDTO>) {
        dailyForecastAdapter.setItems(dailyForecast)
        if (lastSelectedItem == null) {
            dailyForecastAdapter.getOnClick()?.invoke(dailyForecast[0])
        } else {
            lastSelectedItem?.let {
                dailyForecastAdapter.lastSelectedPosition = it
                dailyForecastAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun setWeatherTypeImage(weatherType: String) {
        weatherTypeImageView.setImageResource(IconHelper.getWeatherIconId(weatherType))
    }

    override fun setCityName(lat: Double, lon: Double) {
        locationNameTextView.text = try {
            geocoder.getFromLocation(lat, lon, 1)
                .firstOrNull()?.locality ?: getString(R.string.unknown_location)
        } catch (ex: IOException) {
            getString(R.string.unknown_location)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                val place = PlacePicker.getPlace(requireContext(), data)
                presenter.loadWeatherData(place?.latLng?.latitude, place?.latLng?.longitude)
            }
        }

    }

    override fun showError(errorMessage: String) {
        AlertDialog.Builder(requireContext())
            .setMessage(errorMessage)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }
}