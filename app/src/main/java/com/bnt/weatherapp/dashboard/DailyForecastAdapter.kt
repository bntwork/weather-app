package com.bnt.weatherapp.dashboard

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bnt.weatherapp.DailyForecastDTO
import com.bnt.weatherapp.R
import com.bnt.weatherapp.util.IconHelper
import kotlinx.android.synthetic.main.daily_forecast_item.view.*

class DailyForecastAdapter : RecyclerView.Adapter<DailyForecastAdapter.ViewHolder>() {

    var lastSelectedPosition: Int = 0
    private var list: List<DailyForecastDTO> = ArrayList()
    private var listener: ((DailyForecastDTO) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.daily_forecast_item,
                parent,
                false
            )
        )

    fun setOnItemClickListener(onItemClicked: (DailyForecastDTO) -> Unit) {
        this.listener = onItemClicked
    }

    fun setItems(days: List<DailyForecastDTO>) {
        this.list = days
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val dayForecast = list[position]
        val view = viewHolder.itemView

        view.dayOfWeekTextView.text = dayForecast.dayOfWeek
        view.minMaxTempTextView.text =
            String.format(
                viewHolder.itemView.context.getString(R.string.max_min_temp_format),
                dayForecast.maxTemp,
                dayForecast.minTemp
            )
        view.cloudsImageView.setImageResource(IconHelper.getWeatherIconId(dayForecast.clouds))
        view.isSelected = position == lastSelectedPosition

        view.setOnClickListener {
            lastSelectedPosition = position
            listener?.invoke(dayForecast)
            notifyDataSetChanged()
        }
    }


    fun getOnClick(): ((DailyForecastDTO) -> Unit)? = listener

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}