package com.bnt.weatherapp.exception

import java.io.IOException

class NetworkUnavailableException : IOException() {
    override val message: String = "Network is unavailable"
}