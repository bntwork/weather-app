package com.bnt.weatherapp.search

import com.arellomobile.mvp.InjectViewState
import com.bnt.weatherapp.App
import com.bnt.weatherapp.base.BasePresenter
import com.bnt.weatherapp.util.PrefsHelper
import com.bnt.weatherapp.util.SchedulerProvider
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SearchPresenter : BasePresenter<SearchView>() {
    @Inject
    lateinit var prefsHelper: PrefsHelper
    @Inject
    lateinit var schedulerProvider: SchedulerProvider
    @Inject
    lateinit var router: Router

    init {
        App.appComponent.inject(this)
    }

    fun onCityClicked(lat: Double?, lng: Double?) {
        addDisposable(
            prefsHelper.writeLatLng(lat ?: 0.0, lng ?: 0.0)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        router.exit()
                    },
                    { throwable ->
                        throwable.printStackTrace()
                    }
                )
        )
    }

    fun onBackPressed() {
        router.exit()
    }
}