package com.bnt.weatherapp.search

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bnt.weatherapp.App
import com.bnt.weatherapp.R
import com.bnt.weatherapp.base.BaseFragment
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import kotlinx.android.synthetic.main.fragment_search.*
import java.util.*
import javax.inject.Inject

class SearchFragment : BaseFragment(), SearchView {

    override val layoutId = R.layout.fragment_search
    @Inject
    lateinit var locale: Locale
    @InjectPresenter
    lateinit var presenter: SearchPresenter

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search_menu, menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar1)
        toolbar1.setNavigationOnClickListener { presenter.onBackPressed() }
        setHasOptionsMenu(true)
        citySearchEditText.requestFocus()
        val inputService = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputService.showSoftInput(citySearchEditText, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Places.initialize(requireContext(), getString(R.string.google_places_api_key), locale)
        val client = Places.createClient(requireContext())
        var sessionToken = AutocompleteSessionToken.newInstance()

        val searchAdapter = SearchAdapter()
        searchAdapter.setListener {
            client.fetchPlace(
                FetchPlaceRequest.builder(
                    it,
                    listOf(Place.Field.LAT_LNG)
                )
                    .setSessionToken(sessionToken)
                    .build()
            ).addOnSuccessListener {
                Log.e("klsjdf", it.place.latLng.toString())
                presenter.onCityClicked(
                    it.place.latLng?.latitude,
                    it.place.latLng?.longitude
                )
            }
        }
        searchResultRecyclerView.adapter = searchAdapter

        val requestBuilder = FindAutocompletePredictionsRequest.builder()
            .setSessionToken(sessionToken)
            .setTypeFilter(TypeFilter.CITIES)

        citySearchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                client.findAutocompletePredictions(
                    requestBuilder.setQuery(s.toString())
                        .build()
                ).addOnSuccessListener {
                    searchAdapter.setItems(it.autocompletePredictions)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}