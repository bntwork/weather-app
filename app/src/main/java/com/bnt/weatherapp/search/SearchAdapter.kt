package com.bnt.weatherapp.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bnt.weatherapp.R
import com.google.android.libraries.places.api.model.AutocompletePrediction
import kotlinx.android.synthetic.main.search_item.view.*

class SearchAdapter(
    private var items: List<AutocompletePrediction> = ArrayList()
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private var listener: ((String) -> Unit)? = null

    fun setListener(lst: ((String) -> Unit)) {
        this.listener = lst
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.search_item, parent, false)
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.cityCountryNameTextView.text = items[position].getFullText(null)
        viewHolder.itemView.setOnClickListener {
            listener?.invoke(items[position].placeId)
        }
    }

    fun setItems(items: List<AutocompletePrediction>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}