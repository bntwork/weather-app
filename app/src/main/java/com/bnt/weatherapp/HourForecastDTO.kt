package com.bnt.weatherapp

data class HourForecastDTO(
    val dt: Long,
    val hour: Int,
    val clouds: String,
    val windSpeed: Int,
    val windDirection: Double,
    val humidity: Int,
    val temp: Int
)