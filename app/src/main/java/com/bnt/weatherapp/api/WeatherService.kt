package com.bnt.weatherapp.api

import com.bnt.weatherapp.Const
import com.bnt.weatherapp.api.entity.WeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast")
    fun getForecastByCoords(
        @Query(Const.Query.LAT) latitude: Double,
        @Query(Const.Query.LON) longitude: Double
    ): Single<WeatherResponse>
}