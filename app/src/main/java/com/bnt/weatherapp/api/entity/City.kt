package com.bnt.weatherapp.api.entity


import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("coord")
    val coord: Coord = Coord(),
    @SerializedName("country")
    val country: String = "",
    @SerializedName("timezone")
    val timezone: Int = 0
)