package com.bnt.weatherapp.api.entity


import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("cod")
    val cod: String = "",
    @SerializedName("message")
    val message: Double = 0.0,
    @SerializedName("cnt")
    val cnt: Int = 0,
    @SerializedName("list")
    val list: List<WeathItem> = listOf(),
    @SerializedName("city")
    val city: City = City()
)